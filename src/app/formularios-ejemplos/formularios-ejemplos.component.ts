import { Component, OnInit } from '@angular/core';
import { FormBuilder,  Validators} from '@angular/forms';

@Component({
  selector: 'app-formularios-ejemplos',
  templateUrl: './formularios-ejemplos.component.html',
  styleUrls: ['./formularios-ejemplos.component.css']
})
export class FormulariosEjemplosComponent implements OnInit {
 public loginForm = this.fb.group({
    email: ["", Validators.required],
    password: ["", Validators.required]
  });
  constructor(public fb: FormBuilder) {}
  
  ngOnInit() {
  }

}
