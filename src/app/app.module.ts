import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { DirectivasEjemplosComponent } from './directivas-ejemplos/directivas-ejemplos.component';
import { ServiciosEjemplosComponent } from './servicios-ejemplos/servicios-ejemplos.component';
import { FormulariosEjemplosComponent } from './formularios-ejemplos/formularios-ejemplos.component';
import { RegistroComponent } from './registro/registro.component';

@NgModule({
  declarations: [
    AppComponent,
    DirectivasEjemplosComponent,
    ServiciosEjemplosComponent,
    FormulariosEjemplosComponent,
    RegistroComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
