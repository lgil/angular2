import { Component, OnInit } from '@angular/core';
import { FormBuilder,  Validators} from '@angular/forms';
import {UsuariosService} from '../services/usuarios.services';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css'],
  providers:[UsuariosService]
})
export class RegistroComponent implements OnInit {
  public registroForm = this.fb.group({
    nombre: ["", [Validators.required,Validators.minLength(6)]],
    apellido: ["", Validators.required],
    email: ["", Validators.required],
    telefono: ["", Validators.required],
    direccion: ["", Validators.required],
    password: ["", Validators.required]
  });
  constructor(public fb: FormBuilder,public _UsuariosService:UsuariosService) { }
  registrar(){
    console.log("Registrar");
    this._UsuariosService.registrar(this.registroForm.value);
    
    
  }
  ngOnInit() {
  }

}
