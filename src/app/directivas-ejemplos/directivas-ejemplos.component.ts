import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directivas-ejemplos',
  templateUrl: './directivas-ejemplos.component.html',
  styleUrls: ['./directivas-ejemplos.component.css']
})
export class DirectivasEjemplosComponent implements OnInit {
  clase="css-class";
  ngClass_array=['positivo', 'si'];
  cantidad=1;
  desactivado=false;
  preguntas: string[] = [
  "¿Que día es hoy?",
  "¿Cmo estuvo tu día hoy?",
  "¿Estás aprendiendo Angular 2?"
  ];
  preguntasObj = [
  {
  pregunta: "¿Que día es hoy?",
  si: 22,
  no: 95
  },
  {
  pregunta: "¿Cmo estuvo tu día hoy?",
  si: 262,
  no: 3
  },
  {
  pregunta: "¿Estás aprendiendo Angular 2?",
  si: 1026,
  no: 1
  }
  ];

  ejemplo_ngif=false;
  mostrar(){
    
    this.clase="prueba";
      this.ejemplo_ngif=true;
  }
  

  ngOnInit() {
  }

}
