import { Component, OnInit } from '@angular/core';
//import { Logger } from './services/logger.services';
import {PeppaPigService} from '../services/peppapig.services';
import {LoggerService} from '../services/logger.services';

@Component({
  selector: 'app-servicios-ejemplos',
  templateUrl: './servicios-ejemplos.component.html',
  styleUrls: ['./servicios-ejemplos.component.css'],
  providers:[PeppaPigService,LoggerService]
})
export class ServiciosEjemplosComponent implements OnInit {
  friends:String[];
  constructor(public _peppapigService: PeppaPigService,
  public _LoggerService:LoggerService ) {
    
    this.friends = _peppapigService.getPeppaFriends();
    this._LoggerService.log(this.friends);
   }
   
  ngOnInit() {
  }

}
