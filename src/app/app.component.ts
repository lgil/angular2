import { Component } from '@angular/core';
import { DirectivasEjemplosComponent } from './directivas-ejemplos/directivas-ejemplos.component';
import { ServiciosEjemplosComponent } from './servicios-ejemplos/servicios-ejemplos.component';
import { FormulariosEjemplosComponent } from './formularios-ejemplos/formularios-ejemplos.component';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';
  
  
}
