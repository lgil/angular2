export class LoggerService {
  log(msg: any)   { console.log("log",msg); }
  error(msg: any) { console.error(msg); }
  warn(msg: any)  { console.warn(msg); }
}