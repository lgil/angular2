import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class UsuariosService {
    datos; 
    constructor(private http: Http){}
    
    result: any;

    usuarios(data){
        return this.http.get("/api/user")
            .map(result => result = result.json().data);
        }

    registrar(data){
        console.log(data);
        return this.http.post('/api/user', data)
            .map(res => this.result = res.json())
            .toPromise();
    }
}